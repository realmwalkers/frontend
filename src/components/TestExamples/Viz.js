import React from "react";
import {
  XYPlot,
  LineSeries,
  VerticalGridLines,
  HorizontalGridLines,
  XAxis,
  YAxis,
  DiscreteColorLegend
} from "react-vis";
//https://uber.github.io/react-vis/documentation/getting-started/creating-a-new-react-vis-project
//https://uber.github.io/react-vis/website/dist/storybook/index.html?knob-X%20Axis=true&knob-BarSeries.1.cluster=stack%201&knob-BarSeries.2.cluster=stack%201&knob-BarSeries.3.cluster=stack%201&knob-vertical%20gridlines=true&knob-stroke=%2312939a&knob-horizontal%20gridlines=true&knob-opacity=0.5&knob-curve=curveBasis&knob-fill=%2312939a&knob-style=%7B%7D&knob-colorScale=category&knob-Y%20Axis=true&selectedKind=Series%2FLineSeries%2FBase&selectedStory=Multiple%20Line%20series&full=0&addons=1&stories=1&panelRight=0&addonPanel=kadira%2Fjsx%2Fpanel
//https://uber.github.io/react-vis/documentation/api-reference/axes
//TODO: refactor this to be time series
//https://github.com/uber/react-vis/blob/master/showcase/misc/time-chart.js
class Viz extends React.Component {
  render() {
    const user1Physical = [
      { x: 1, y: 8 },
      { x: 2, y: 6 },
      { x: 3, y: 4 },
      { x: 4, y: 10 },
      { x: 5, y: 1 }
    ];
    const user1Mental = [
      { x: 1, y: 8 },
      { x: 2, y: 3 },
      { x: 3, y: 4 },
      { x: 4, y: 9 },
      { x: 5, y: 1 }
    ];
    const user1Goal = [
      { x: 1, y: 8 },
      { x: 2, y: 4 },
      { x: 3, y: 4 },
      { x: 4, y: 9 },
      { x: 5, y: 1 }
    ];
    const user2Physical = [
      { x: 1, y: 1 },
      { x: 2, y: 3 },
      { x: 3, y: 4 },
      { x: 4, y: 10 },
      { x: 5, y: 6 }
    ];
    const user2Mental = [
      { x: 1, y: 8 },
      { x: 2, y: 3 },
      { x: 3, y: 7 },
      { x: 4, y: 4 },
      { x: 5, y: 6 }
    ];
    const user2Goal = [
      { x: 1, y: 6 },
      { x: 2, y: 5 },
      { x: 3, y: 3 },
      { x: 4, y: 4 },
      { x: 5, y: 8 }
    ];
    const days = ["Mon", "Tue", "Wed", "Thu", "Fri"];
    return (
      <>
        <p>User 1</p>
        <div style={{ display: "flex", flexDirection: "row" }}>
          <DiscreteColorLegend
            colors={["#19CDD7", "#DDB27C", "#88572C"]}
            items={["physical", "mental", "goals"]}
            orientation="vertical"
          />
          <XYPlot height={300} width={500}>
            <VerticalGridLines />
            <HorizontalGridLines />
            {/* <XAxis title="X axis" /> */}
            <XAxis
              hideline
              tickFormat={v => `${days[v - 1]}`}
              tickValues={[1, 2, 3, 4, 5]}
              // tickTotal={data.length}
            />
            <YAxis tickValues={[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]} />
            <LineSeries data={user1Physical} color="#DDB27C" />
            <LineSeries data={user1Mental} />
            <LineSeries data={user1Goal} color="#88572C" />
          </XYPlot>
        </div>
        <p>User 2</p>
        <div style={{ display: "flex", flexDirection: "row" }}>
          <DiscreteColorLegend
            colors={["#19CDD7", "#DDB27C", "#88572C"]}
            items={["physical", "mental", "goals"]}
            orientation="vertical"
          />
          <XYPlot height={300} width={500}>
            <VerticalGridLines />
            <HorizontalGridLines />
            {/* <XAxis title="X axis" /> */}
            <XAxis
              hideline
              tickFormat={v => `${days[v - 1]}`}
              tickValues={[1, 2, 3, 4, 5]}
              // tickTotal={data.length}
            />
            <YAxis tickValues={[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]} />
            <LineSeries data={user2Physical} color="#DDB27C" />
            <LineSeries data={user2Mental} />
            <LineSeries data={user2Goal} color="#88572C" />
          </XYPlot>
        </div>
      </>
    );
  }
}

export default Viz;
