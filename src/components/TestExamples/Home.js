import React from "react";
import { Link } from "react-router-dom";

export default class Home extends React.Component {
  state = {};

  render() {
    return (
      <>
        <div
          style={{
            display: "flex",
            flexDirection: "column"
          }}
        >
          <p>home</p>
          <Link to="/login">click here to login </Link>
          <Link to="/register">click here to register </Link>
          <Link to="/dashboard">click here to go to the dashboard </Link>
          <Link to="/assessment">click here to go to assessment </Link>
          <Link to="/viz">click here to test user viz </Link>
        </div>
      </>
    );
  }
}
