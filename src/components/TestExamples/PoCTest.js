import React from "react";
import { Link } from "react-router-dom";
export default class Dashboard extends React.Component {
  state = { login: "", register: "", dataGet: "", dataPost: "" };
  async componentDidMount() {
    let loginBody = {
      user: "user",
      password: "pass"
    };
    let loginResp = await fetch("http://localhost:3000/auth/login", {
      headers: {
        "Content-Type": "application/json"
      },
      method: "POST",
      body: JSON.stringify(loginBody)
    });
    let registerBody = {
      user: "user" + Math.random(),
      password: "pass"
    };
    let registerResp = await fetch("http://localhost:3000/auth/register", {
      headers: {
        "Content-Type": "application/json"
      },
      method: "POST",
      body: JSON.stringify(registerBody)
    });
    let dataBody = {
      health: 32,
      physical: 2,
      goal: 3
    };
    let dataPostResp = await fetch("http://localhost:3000/data", {
      headers: {
        "Content-Type": "application/json"
      },
      method: "POST",
      body: JSON.stringify(dataBody)
    });
    let dataGetResp = await fetch("http://localhost:3000/data");
    console.log(await registerResp.json());
    console.log(await loginResp.json());
    console.log(await dataGetResp.json());
    console.log(await dataPostResp.json());
    this.setState({
      login: "login successful",
      register: "register succesful",
      dataGet: "data GET successful",
      dataPost: "data POST successful"
    });
  }
  render() {
    return (
      <>
        <p>Proof of Concept Test Page</p>
        <p>{this.state.login}</p>
        <p>{this.state.register}</p>
        <p>{this.state.dataGet}</p>
        <p>{this.state.dataPost}</p>
        <Link to="/">return home </Link>
      </>
    );
  }
}
