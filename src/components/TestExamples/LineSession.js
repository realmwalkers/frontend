import React from "react";
import {
  LineChart,
  LineSeries,
  Line,
  LinearXAxis,
  LinearYAxis,
  LinearXAxisTickSeries,
  LinearYAxisTickSeries,
  DiscreteLegend,
  DiscreteLegendEntry
} from "reaviz";
import { timeDay } from "d3-time";
export default class ReavizLineSession extends React.Component {
  state = {
    data: null,
    isDataFetched: false
  };

  async componentDidMount() {
    let perfResults = await fetch("http://68.183.61.205:3000/data", {
      credentials: "include"
    });

    if (perfResults.status === 200) {
      let result = await perfResults.json();
      let physicalData = this.getReavizData(result.physical);
      let mentalData = this.getReavizData(result.mental);
      let goalData = this.getReavizData(result.goal);
      let groupedData = [
        { key: "mental", data: mentalData },
        { key: "physical", data: physicalData },
        { key: "goal", data: goalData }
      ];
      console.log(groupedData);
      await this.setState({ data: groupedData, isDataFetched: true });
    }
  }

  getReavizData = scoreArr => {
    let newArr = [];
    for (var i = 0; i < scoreArr.length; i++) {
      let assessment = scoreArr[i];
      let obj = {
        key: new Date(assessment.date),
        data: assessment.score
      };
      newArr.push(obj);
    }
    return newArr;
  };

  render() {
    if (this.state.isDataFetched) {
      return (
        <>
          <DiscreteLegend
            style={{ width: "200px" }}
            entries={[
              <DiscreteLegendEntry label="Physical" color="#6CCFF6" />,
              <DiscreteLegendEntry label="Mental" color="#EA3788" />,
              <DiscreteLegendEntry label="Goal" color="#119DA4" />
            ]}
          />
          <LineChart
            width={550}
            height={450}
            data={this.state.data}
            series={
              <LineSeries
                type="grouped"
                interpolation="linear"
                // colorScheme={"cybertron"}
                colorScheme={["#6CCFF6", "#EA3788", "#119DA4"]}
                line={<Line strokeWidth={4} />}
              />
            }
            yAxis={
              <LinearYAxis
                type="value"
                tickSeries={<LinearYAxisTickSeries />}
                domain={[0, 10]}
                scaled="false"
              />
            }
            xAxis={
              <LinearXAxis
                type="time"
                tickSeries={<LinearXAxisTickSeries interval={timeDay} />}
              />
            }
          />
        </>
      );
    } else {
      return <p>no data to display </p>;
    }
  }
}
