import React from "react";
import moment from "moment";
import {
  XYPlot,
  XAxis,
  YAxis,
  HorizontalGridLines,
  VerticalGridLines,
  LineSeries,
  DiscreteColorLegend
} from "react-vis";
//https://github.com/uber/react-vis/blob/master/showcase/misc/time-chart.js
export default class TimeSeriesViz extends React.Component {
  state = {
    physicalArray: [],
    mentalArray: [],
    goalArray: []
  };

  async componentDidMount() {
    let dataResp = await fetch("http://localhost:3000/data", {
      credentials: "include"
    });
    let dataRespJson = await dataResp.json();
    console.log(dataRespJson);
    this.setState({
      physicalArray: dataRespJson.physical,
      mentalArray: dataRespJson.mental,
      goalArray: dataRespJson.goal
    });
  }

  getXYFromPhysicalArray = () => {
    let newArr = this.state.physicalArray.map(entry => ({
      x: new Date(entry.date),
      y: entry.physical_score
    }));
    return newArr;
  };

  getXYFromMentalArray = () => {
    let newArr = this.state.mentalArry.map(entry => ({
      x: new Date(entry.date),
      y: entry.health_score
    }));
    return newArr;
  };

  getXYFromGoalArray = () => {
    let newArr = this.state.goalArray.map(entry => ({
      x: new Date(entry.date),
      y: entry.goal_score
    }));
    return newArr;
  };

  // getXYFromArrayMock = () => {
  //   let mockArr = [];
  //   for (var i = 0; i < 5; i++) {
  //     let newEntry = {
  //       x: new Date("2019-10-28T22:13:22.007Z"),
  //       y: Math.floor(Math.random() * 10) + 1
  //     };
  //     mockArr.push(newEntry);
  //   }
  //   return mockArr;
  // };

  // getXYFromArrayMock = () => {
  //   let mockArr = [
  //     {
  //       x: new Date("10/28/19"),
  //       y: Math.floor(Math.random() * 10) + 1
  //     },
  //     {
  //       x: new Date("10/29/19"),
  //       y: Math.floor(Math.random() * 10) + 1
  //     },
  //     {
  //       x: new Date("10/30/19"),
  //       y: Math.floor(Math.random() * 10) + 1
  //     },
  //     {
  //       x: new Date("10/31/19"),
  //       y: Math.floor(Math.random() * 10) + 1
  //     },
  //     {
  //       x: new Date("11/1/19"),
  //       y: Math.floor(Math.random() * 10) + 1
  //     }
  //   ];
  //   return mockArr;
  // };

  getXYFromArrayMock = () => {
    let mockArr = [
      {
        x: new Date("2019-10-28T22:13:22.007Z"),
        y: Math.floor(Math.random() * 10) + 1
      },
      {
        x: new Date("2019-10-29T22:13:22.007Z"),
        y: Math.floor(Math.random() * 10) + 1
      },
      {
        x: new Date("2019-10-30T22:13:22.007Z"),
        y: Math.floor(Math.random() * 10) + 1
      },
      {
        x: new Date("2019-10-31T22:13:22.007Z"),
        y: Math.floor(Math.random() * 10) + 1
      },
      {
        x: new Date("2019-11-01T22:13:22.007Z"),
        y: Math.floor(Math.random() * 10) + 1
      }
    ];
    return mockArr;
  };

  render() {
    return (
      <div style={{ display: "flex", flexDirection: "row" }}>
        <DiscreteColorLegend
          colors={["#f4d35e", "#114b5f", "#ef2d56"]}
          items={["physical", "mental", "goals"]}
          orientation="vertical"
        />
        <XYPlot
          xType="ordinal"
          width={500}
          height={300}
          animation={{ damping: 9, stiffness: 300 }}
        >
          <HorizontalGridLines />
          <VerticalGridLines />
          <XAxis
            title="X Axis"
            tickFormat={function tickFormat(d) {
              return moment(d).format("ddd");
            }}
            tickTotal={5}
          />
          <YAxis title="Y Axis" yDomain={[1, 10]} />
          <LineSeries data={this.getXYFromArrayMock()} color="#f4d35e" />
          <LineSeries data={this.getXYFromArrayMock()} color="#114b5f" />
          <LineSeries data={this.getXYFromArrayMock()} color="#ef2d56" />
        </XYPlot>
      </div>
    );
  }
}
