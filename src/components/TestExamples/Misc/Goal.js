import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import FullScreenDialogueCard from "./FullScreenDialogueCard";
import FullScreenDialogueFab from "./FullScreenDialogueFab";
import Fab from "./Fab";
const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    paddingTop: "16px"
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary
  }
}));

export default function CenteredGrid() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Grid container spacing={3}>
        <Grid item xs={3}>
          <FullScreenDialogueCard />
        </Grid>
        <Grid item xs={3}>
          <FullScreenDialogueFab />
        </Grid>
      </Grid>
    </div>
  );
}
