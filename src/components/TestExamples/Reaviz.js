import React from "react";
import {
  AreaChart,
  AreaSeries,
  Line,
  LinearXAxis,
  LinearYAxis,
  LinearXAxisTickSeries,
  LinearYAxisTickSeries
} from "reaviz";
import { timeDay } from "d3-time";
const ReavizTest = () => {
  const data = [
    {
      key: new Date("2019-10-28T22:13:22.007Z"),
      data: Math.floor(Math.random() * 10) + 1
    },
    {
      key: new Date("2019-10-29T22:13:22.007Z"),
      data: Math.floor(Math.random() * 10) + 1
    },
    {
      key: new Date("2019-10-30T22:13:22.007Z"),
      data: Math.floor(Math.random() * 10) + 1
    },
    {
      key: new Date("2019-10-31T22:13:22.007Z"),
      data: Math.floor(Math.random() * 10) + 1
    },
    {
      key: new Date("2019-11-01T22:13:22.007Z"),
      data: Math.floor(Math.random() * 10) + 1
    }
  ];
  return (
    <AreaChart
      width={550}
      height={450}
      data={data}
      series={
        <AreaSeries
          interpolation={{
            linear: "linear",
            step: "step",
            smooth: "smooth"
          }}
          colorScheme={"cybertron"}
          line={<Line strokeWidth={4} />}
        />
      }
      yAxis={
        <LinearYAxis
          type="value"
          tickSeries={<LinearYAxisTickSeries />}
          domain={[0, 10]}
          scaled="false"
        />
      }
      xAxis={
        <LinearXAxis
          type="time"
          tickSeries={<LinearXAxisTickSeries interval={timeDay} />}
        />
      }
    />
    // <AreaChart
    //   width={550}
    //   height={450}
    //   data={data}
    //   series={
    //     <AreaSeries
    //       interpolation={{
    //         linear: "linear",
    //         step: "step",
    //         smooth: "smooth"
    //       }}
    //       colorScheme={"cybertron"}
    //       line={<Line strokeWidth={4} />}
    //     />
    //   }
    // />
  );
};
export default ReavizTest;
