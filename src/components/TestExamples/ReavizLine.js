import React from "react";
import {
  LineChart,
  LineSeries,
  Line,
  LinearXAxis,
  LinearYAxis,
  LinearXAxisTickSeries,
  LinearYAxisTickSeries,
  DiscreteLegend,
  DiscreteLegendEntry
} from "reaviz";
import { timeDay } from "d3-time";
const ReavizLine = () => {
  const data = [
    {
      key: new Date("2019-10-28T22:13:22.007Z"),
      data: Math.floor(Math.random() * 10) + 1
    },
    {
      key: new Date("2019-10-29T22:13:22.007Z"),
      data: Math.floor(Math.random() * 10) + 1
    },
    {
      key: new Date("2019-10-30T22:13:22.007Z"),
      data: Math.floor(Math.random() * 10) + 1
    },
    {
      key: new Date("2019-10-31T22:13:22.007Z"),
      data: Math.floor(Math.random() * 10) + 1
    },
    {
      key: new Date("2019-11-01T22:13:22.007Z"),
      data: Math.floor(Math.random() * 10) + 1
    }
  ];

  const groupedData = [
    {
      key: "mental",
      data: [
        {
          key: new Date("2019-10-28T22:13:22.007Z"),
          data: Math.floor(Math.random() * 10) + 1
        },
        {
          key: new Date("2019-10-29T22:13:22.007Z"),
          data: Math.floor(Math.random() * 10) + 1
        },
        {
          key: new Date("2019-10-30T22:13:22.007Z"),
          data: Math.floor(Math.random() * 10) + 1
        },
        {
          key: new Date("2019-10-31T22:13:22.007Z"),
          data: Math.floor(Math.random() * 10) + 1
        },
        {
          key: new Date("2019-11-01T22:13:22.007Z"),
          data: Math.floor(Math.random() * 10) + 1
        }
      ]
    },
    {
      key: "physical",
      data: [
        {
          key: new Date("2019-10-28T22:13:22.007Z"),
          data: Math.floor(Math.random() * 10) + 1
        },
        {
          key: new Date("2019-10-29T22:13:22.007Z"),
          data: Math.floor(Math.random() * 10) + 1
        },
        {
          key: new Date("2019-10-30T22:13:22.007Z"),
          data: Math.floor(Math.random() * 10) + 1
        },
        {
          key: new Date("2019-10-31T22:13:22.007Z"),
          data: Math.floor(Math.random() * 10) + 1
        },
        {
          key: new Date("2019-11-01T22:13:22.007Z"),
          data: Math.floor(Math.random() * 10) + 1
        }
      ]
    },
    {
      key: "goal",
      data: [
        {
          key: new Date("2019-10-28T22:13:22.007Z"),
          data: Math.floor(Math.random() * 10) + 1
        },
        {
          key: new Date("2019-10-29T22:13:22.007Z"),
          data: Math.floor(Math.random() * 10) + 1
        },
        {
          key: new Date("2019-10-30T22:13:22.007Z"),
          data: Math.floor(Math.random() * 10) + 1
        },
        {
          key: new Date("2019-10-31T22:13:22.007Z"),
          data: Math.floor(Math.random() * 10) + 1
        },
        {
          key: new Date("2019-11-01T22:13:22.007Z"),
          data: Math.floor(Math.random() * 10) + 1
        }
      ]
    }
  ];
  return (
    <>
      <DiscreteLegend
        style={{ width: "200px" }}
        entries={[
          <DiscreteLegendEntry label="Physical" color="#6CCFF6" />,
          <DiscreteLegendEntry label="Mental" color="#EA3788" />,
          <DiscreteLegendEntry label="Goal" color="#119DA4" />
        ]}
      />
      <LineChart
        width={550}
        height={450}
        data={groupedData}
        series={
          <LineSeries
            type="grouped"
            interpolation={{
              linear: "linear",
              step: "step",
              smooth: "smooth"
            }}
            // colorScheme={"cybertron"}
            colorScheme={["#6CCFF6", "#EA3788", "#119DA4"]}
            line={<Line strokeWidth={4} />}
          />
        }
        yAxis={
          <LinearYAxis
            type="value"
            tickSeries={<LinearYAxisTickSeries />}
            domain={[0, 10]}
            scaled="false"
          />
        }
        xAxis={
          <LinearXAxis
            type="time"
            tickSeries={<LinearXAxisTickSeries interval={timeDay} />}
          />
        }
      />
    </>
  );
};
export default ReavizLine;
