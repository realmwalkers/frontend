import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import StepContent from "@material-ui/core/StepContent";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
const useStyles = makeStyles(theme => ({
  root: {
    width: "100%"
  },
  button: {
    marginTop: theme.spacing(1),
    marginRight: theme.spacing(1),
    alignItems: "center"
  },
  actionsContainer: {
    marginBottom: theme.spacing(2)
  },
  resetContainer: {
    padding: theme.spacing(3)
  }
}));

export default function Assessment(props) {
  const classes = useStyles();
  const steps = props.dimensionsTemplate;
  const [activeStep, setActiveStep] = React.useState(0);
  const [currAnswer, setCurrAnswer] = React.useState({
    dimensionName: "",
    dimensionScore: ""
  });
  const [comment, setComment] = React.useState("");
  const [results, setResults] = React.useState([]);

  const handleChange = event => {
    setCurrAnswer({
      dimensionName: event.target.name,
      dimensionScore: Number.parseInt(event.target.value)
    });
  };

  const handleCommentChange = event => {
    console.log(event.target.value);
    setComment(event.target.value);
  };

  const handleNext = () => {
    setResults([...results, currAnswer]);
    setCurrAnswer({ dimensionName: "", dimensionScore: "" });
    setActiveStep(prevActiveStep => prevActiveStep + 1);
  };

  const handleCommentNext = () => {
    setActiveStep(prevActiveStep => prevActiveStep + 1);
  };

  const handleSubmit = async () => {
    let postBody = {
      categoryId: props.categoryId,
      assessmentResult: results,
      assessmentComment: comment
    };
    console.log(postBody);
    let assessmentResp = await fetch(
      `${process.env.REACT_APP_API_URL}/assessment/entry`,
      {
        credentials: "include",
        headers: {
          "Content-Type": "application/json"
        },
        method: "POST",
        body: JSON.stringify(postBody)
      }
    );
    props.handleClose();
    props.onCategoryChange();
  };

  return (
    <div className={classes.root}>
      <Stepper activeStep={activeStep} orientation="vertical">
        {steps.map((step, index) => (
          <Step key={step._id}>
            <StepLabel>{`Question ${index + 1}`}</StepLabel>
            <StepContent>
              <Typography>{`In general, how would you rate yourself on: ${step.dimensionName}`}</Typography>
              <div className={classes.actionsContainer}>
                <TextField
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  id={step._id}
                  label="Rating (1-10)"
                  name={step.dimensionName}
                  autoFocus
                  onChange={handleChange}
                />
                <div>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={handleNext}
                    className={classes.button}
                    disabled={currAnswer.dimensionScore === ""}
                  >
                    {activeStep === steps.length ? "Finish" : "Next"}
                  </Button>
                </div>
              </div>
            </StepContent>
          </Step>
        ))}
        <Step>
          <StepLabel>{`Comment`}</StepLabel>
          <StepContent>
            <Typography>
              {"Would you like to note anything for this assessment?"}
            </Typography>
            <div className={classes.actionsContainer}>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                label="Comment"
                autoFocus
                onChange={handleCommentChange}
              />
              <div>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={handleCommentNext}
                  className={classes.button}
                >
                  {activeStep === steps.length + 1 ? "Finish" : "Next"}
                </Button>
              </div>
            </div>
          </StepContent>
        </Step>
      </Stepper>
      {activeStep === steps.length + 1 && (
        <Paper square elevation={0} className={classes.resetContainer}>
          <Typography>{}</Typography>
          <Button onClick={handleSubmit} className={classes.button}>
            Submit
          </Button>
        </Paper>
      )}
    </div>
  );
}
