import React from "react";
import Card from "./Card";
import { makeStyles, Container, Grid } from "@material-ui/core";
import NewCategoryButton from "./NewCategoryButton";

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex"
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: "100vh",
    overflow: "auto"
  },
  container: {
    marginLeft: 57,
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4)
  }
}));

export default function Categories(props) {
  const classes = useStyles();
  const [categories, setCategories] = React.useState([]);
  React.useEffect(() => {
    fetchCategories();
  }, []);

  const fetchCategories = async () => {
    let fetchCategoryResp = await fetch(
      `${process.env.REACT_APP_API_URL}/category`,
      {
        credentials: "include",
        headers: {
          "Content-Type": "application/json"
        }
      }
    );
    if (fetchCategoryResp.ok) {
      setCategories(await fetchCategoryResp.json());
    }
  };

  const onCategoryChange = async () => {
    fetchCategories();
  };
  return (
    <>
      <div className={classes.appBarSpacer}>
        <Container maxWidth="lg" className={classes.container}>
          <Grid container spacing={3}>
            {categories.map(cat => {
              return (
                <Grid item xs={3}>
                  <Card
                    key={cat._id}
                    categoryName={cat.categoryName}
                    dimensionsTemplate={cat.dimensionsTemplate}
                    categoryId={cat.categoryId}
                    description={cat.categoryDescription}
                    dimensionScore={cat.categoryScoreCurrent}
                    categoryScore={cat.categoryScorePossible}
                    onCategoryChange={onCategoryChange}
                    {...props}
                  />
                </Grid>
              );
            })}
            <Grid item xs={3} container alignItems="center" justify="center">
              <NewCategoryButton onCategoryChange={onCategoryChange} />
            </Grid>
          </Grid>
        </Container>
      </div>
    </>
  );
}
