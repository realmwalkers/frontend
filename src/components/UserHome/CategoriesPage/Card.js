import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import TakeAssessmentButton from "../Assessment/TakeAssessmentButton";

const useStyles = makeStyles({
  card: {
    maxWidth: 345
  },
  media: {
    height: 140
  }
});

export default function MediaCard(props) {
  const classes = useStyles();

  const handleRedirect = () => {
    console.log("click");
    props.history.push(`/dashboard/${props.categoryName}/${props.categoryId}`);
  };

  return (
    <Card className={classes.card}>
      <CardContent>
        <Typography gutterBottom variant="h5" component="h2">
          {props.categoryName}
        </Typography>
        <Typography component="p" variant="h3">
          {(props.dimensionScore / props.categoryScore) * 100
            ? ((props.dimensionScore / props.categoryScore) * 100).toFixed(0)
            : "0"}
          %
        </Typography>
        <Typography variant="body2" color="textSecondary" component="p">
          {props.description}
        </Typography>
      </CardContent>
      <CardActions>
        <Button size="small" color="primary" onClick={handleRedirect}>
          Dashboard
        </Button>
        <TakeAssessmentButton
          dimensionsTemplate={props.dimensionsTemplate}
          categoryId={props.categoryId}
          onCategoryChange={props.onCategoryChange}
        />
      </CardActions>
    </Card>
  );
}
