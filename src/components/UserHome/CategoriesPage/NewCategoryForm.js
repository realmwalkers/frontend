import React from "react";
import { FormGroup, TextField, Button } from "@material-ui/core";

export default class NewCategoryForm extends React.Component {
  handleSubmit = async event => {
    event.preventDefault();
    let catName = event.target.categoryName.value;
    let firstDim = event.target.firstDim.value;
    let secondDim = event.target.secondDim.value;
    let categoryDesc = event.target.categoryDesc.value;
    console.log(categoryDesc);
    let postBody = {
      categoryId: Math.floor(Math.random() * 1000000000),
      categoryName: catName,
      categoryDescription: categoryDesc,
      dimensionList: [
        { dimensionName: firstDim, dimensionDiscription: "this a dis 1" },
        { dimensionName: secondDim, dimensionDiscription: "this a dis 2" }
      ]
    };
    let dataResp = await fetch(
      `${process.env.REACT_APP_API_URL}/category/new`,
      {
        credentials: "include",
        headers: {
          "Content-Type": "application/json"
        },
        method: "POST",
        body: JSON.stringify(postBody)
      }
    );
    if (dataResp.status === 200) {
      console.log(dataResp);
    }
    this.props.onCategoryChange();
    this.props.handleClose();
  };
  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <FormGroup>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="categoryName"
            label="Category Name"
            name="categoryName"
            autoFocus
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="categoryDesc"
            label="Category Description"
            name="categoryDesc"
            autoFocus
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="Dimension"
            label="Dimension Name"
            name="firstDim"
            autoFocus
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="Dimension"
            label="Dimension Name"
            name="secondDim"
            autoFocus
          />
          <Button type="submit">Save Category</Button>
        </FormGroup>
      </form>
    );
  }
}
