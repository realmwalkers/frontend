import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import StepContent from "@material-ui/core/StepContent";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
const useStyles = makeStyles(theme => ({
  root: {
    width: "100%"
  },
  button: {
    marginTop: theme.spacing(1),
    marginRight: theme.spacing(1),
    alignItems: "center"
  },
  actionsContainer: {
    marginBottom: theme.spacing(2)
  },
  resetContainer: {
    padding: theme.spacing(3)
  }
}));

export default function NewCategoryStepper(props) {
  const classes = useStyles();
  const initialSteps = [
    {
      name: "categoryName",
      title: "Name your category",
      content: "What 'life category' would you like to be more aware of?",
      label: "Name"
    },
    {
      name: "categoryDescription",
      title: "Describe your category",
      content: "Provide a brief description.",
      label: "Description"
    },
    {
      name: "numberOfDimensions",
      title: "Number of Dimensions",
      content: "How many dimensions are there of this category? (1-5)",
      label: "Number"
    }
  ];
  const [steps, setSteps] = React.useState(initialSteps);
  const [activeStep, setActiveStep] = React.useState(0);
  const [currAnswer, setCurrAnswer] = React.useState({
    name: "",
    value: ""
  });
  var [apiPayload, setApiPayload] = React.useState({
    categoryId: Math.floor(Math.random() * 1000000000),
    categoryName: null,
    categoryDescription: null,
    dimensionList: []
  });

  const handleChange = event => {
    // console.log(event.target.name);
    // console.log(event.target.value);
    setCurrAnswer({
      name: event.target.name,
      value: event.target.value
    });
    console.log(currAnswer);
  };

  const handleNext = () => {
    if (currAnswer.name === "numberOfDimensions") {
      generateDimensionSteps(currAnswer.value);
    } else if (currAnswer.name === "categoryName") {
      setApiPayload({ ...apiPayload, categoryName: currAnswer.value });
    } else if (currAnswer.name === "categoryDescription") {
      setApiPayload({ ...apiPayload, categoryDescription: currAnswer.value });
    } else {
      setApiPayload({
        ...apiPayload,
        dimensionList: [
          ...apiPayload.dimensionList,
          {
            dimensionName: currAnswer.value,
            dimensionDiscription: currAnswer.value
          }
        ]
      });
    }
    setCurrAnswer({ name: "", value: "" });
    setActiveStep(prevActiveStep => prevActiveStep + 1);
    console.log(apiPayload);
  };

  const updatePayload = () => {};

  const generateDimensionSteps = numDimensions => {
    let stepsToAdd = [];
    for (var i = 1; i <= numDimensions; i++) {
      let newStep = {
        name: `Dimension${i}`,
        title: `Dimension ${i}`,
        content: `Enter a name for Dimension ${i}`,
        label: `Name`
      };
      stepsToAdd.push(newStep);
    }
    setSteps([...steps, ...stepsToAdd]);
  };

  const handleSubmit = async () => {
    let dataResp = await fetch(
      `${process.env.REACT_APP_API_URL}/category/new`,
      {
        credentials: "include",
        headers: {
          "Content-Type": "application/json"
        },
        method: "POST",
        body: JSON.stringify(apiPayload)
      }
    );
    if (dataResp.status === 200) {
      console.log(await dataResp.json());
    }
    props.handleClose();
    props.onCategoryChange();
  };

  return (
    <div className={classes.root}>
      <Stepper activeStep={activeStep} orientation="vertical">
        {steps.map((step, index) => (
          <Step key={Math.random()}>
            <StepLabel>{step.title}</StepLabel>
            <StepContent>
              <Typography>{step.content}</Typography>
              <div className={classes.actionsContainer}>
                <TextField
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  // id={step.id}
                  label={step.label}
                  name={step.name}
                  value={currAnswer.value}
                  autoFocus
                  onChange={handleChange}
                />
                <div>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={handleNext}
                    className={classes.button}
                    disabled={currAnswer.value === ""}
                  >
                    {activeStep === steps.length ? "Finish" : "Next"}
                  </Button>
                </div>
              </div>
            </StepContent>
          </Step>
        ))}
      </Stepper>
      {activeStep === steps.length && (
        <Paper square elevation={0} className={classes.resetContainer}>
          <Typography>{}</Typography>
          <Button onClick={handleSubmit} className={classes.button}>
            Submit
          </Button>
        </Paper>
      )}
    </div>
  );
}
