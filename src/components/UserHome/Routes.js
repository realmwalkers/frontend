import React from "react";
import { Home, Category, History, Help } from "@material-ui/icons";
import Dashboard from "./Dashboard/Dashboard";
import HistoryPage from "./History/History";
import PromisContainer from "./PromisApi/PromisApiContainer";
import Categories from "./CategoriesPage/Categories";
const Routes = [
  {
    path: "/dashboard",
    primary: "Dashboard",
    icon: <Home />,
    component: Dashboard
  },
  {
    path: "/categories",
    primary: "Categories",
    icon: <Category />,
    component: Categories
  },
  {
    path: "/history",
    primary: "History",
    icon: <History />,
    component: HistoryPage
  },
  {
    path: "/promis",
    primary: "Promis API",
    icon: <Help />,
    component: PromisContainer
  }
];

export default Routes;
