import React from "react";
import DrawerNav from "./DrawerNav";
import DashboardContainer from "./Dashboard/DashboardContainer";
import Categories from "./CategoriesPage/Categories";
import { ProtectedRoute } from "../../protected-route";
import Route from "react-router-dom/Route";
import HistoryContainer from "./History/HistoryContainer";
import PromisContainer from "./PromisApi/PromisApiContainer";
export const UserHomeContainer = props => (
  <div>
    <div className="container">
      <DrawerNav {...props} />
      <ProtectedRoute
        path="/dashboard/:categoryName?/:categoryId?"
        component={DashboardContainer}
      />
      <ProtectedRoute exact path="/categories" component={Categories} />
      <ProtectedRoute
        exact
        path="/history/:date?/:rating?"
        component={HistoryContainer}
      />
      <ProtectedRoute exact path="/promis" component={PromisContainer} />
    </div>
  </div>
);
