import React from "react"
import { makeStyles } from "@material-ui/core/styles"
import History from "./History"
import Button from "@material-ui/core/Button"
import CloudDownloadIcon from "@material-ui/icons/CloudDownload"
import { CSVLink } from "react-csv"

export default class HistoryContainer extends React.Component {
  state = {
    fetchResult: null,
    csvFile: []
  }

  async componentDidMount() {
    await this.fetchData()
  }

  fetchData = async () => {
    let fetchHistoryData = await fetch(
      `${process.env.REACT_APP_API_URL}/category`,
      {
        credentials: "include"
      }
    )
    if (fetchHistoryData.ok) {
      let result = await fetchHistoryData.json()
      await this.setState({
        fetchResult: result
      })
    }
    // console.log(this.state.fetchResult[0])
    var assessmentObjectArray = []
    for (
      let i = 0;
      i < this.state.fetchResult[0].assessmentsResults.length;
      i++
    ) {
      var assessmentObject = {
        categoryName: this.state.fetchResult[0].assessmentsResults[i]
          .categoryName,
        assessmentDate: this.state.fetchResult[0].assessmentsResults[i]
          .assessmentDate,
        assessmentComment: this.state.fetchResult[0].assessmentsResults[i]
          .assessmentComment,
        assessmentScore: this.state.fetchResult[0].assessmentsResults[i]
          .assessmentScore
      }
      assessmentObjectArray[i] = assessmentObject
    }
    this.setState({
      csvFile: assessmentObjectArray
    })
  }

  render() {
    const useStyles = makeStyles((theme) => ({
      button: {
        margin: theme.spacing(1)
      }
    }))

    if (this.state.fetchResult !== null) {
      return (
        <>
          <History
            {...this.props}
            fetchResult={this.state.fetchResult}
            searchText={this.props.match.params.date}
          />
          <CSVLink
            style={{ textDecoration: "none" }}
            data={this.state.csvFile}
            // I also tried adding the onClick event on the link itself
            filename={"dataTrace.csv"}
            target='_blank'
          >
            <Button
              variant='contained'
              color='primary'
              className={useStyles.button}
              endIcon={<CloudDownloadIcon />}
            >
              Download
            </Button>
          </CSVLink>
        </>
      )
    } else {
      return <div />
    }
  }
}
