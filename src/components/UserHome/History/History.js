import React, { Component } from "react";
import MaterialTable from "material-table";
import { Grid, Container, Paper, makeStyles } from "@material-ui/core";
import clsx from "clsx";

import { forwardRef } from "react";

import AddBox from "@material-ui/icons/AddBox";
import ArrowDownward from "@material-ui/icons/ArrowDownward";
import Check from "@material-ui/icons/Check";
import ChevronLeft from "@material-ui/icons/ChevronLeft";
import ChevronRight from "@material-ui/icons/ChevronRight";
import Clear from "@material-ui/icons/Clear";
import DeleteOutline from "@material-ui/icons/DeleteOutline";
import Edit from "@material-ui/icons/Edit";
import FilterList from "@material-ui/icons/FilterList";
import FirstPage from "@material-ui/icons/FirstPage";
import LastPage from "@material-ui/icons/LastPage";
import Remove from "@material-ui/icons/Remove";
import SaveAlt from "@material-ui/icons/SaveAlt";
import Search from "@material-ui/icons/Search";
import ViewColumn from "@material-ui/icons/ViewColumn";

const tableIcons = {
  Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
  Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
  Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
  DetailPanel: forwardRef((props, ref) => (
    <ChevronRight {...props} ref={ref} />
  )),
  Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
  Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
  Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
  FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
  LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
  NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  PreviousPage: forwardRef((props, ref) => (
    <ChevronLeft {...props} ref={ref} />
  )),
  ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
  SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
  ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
  ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
};

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex"
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: "100vh",
    overflow: "auto"
  },
  container: {
    marginLeft: 57,
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4)
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column"
  },
  graphPaperHeight: {
    height: 500 - 24
  },
  fixedHeightPaper: {
    height: 250 - 24
  }
}));

export default function History(props) {
  const fitData = categoriesArray => {
    if (categoriesArray.length === 0) {
      return [];
    }

    /*
     *  MaterialTable take an array on objects
     *  return one array of objects with specified fields
     */
    // Of each category, add category name to each assessment
    for (let i = 0; i < categoriesArray.length; i++) {
      for (let j = 0; j < categoriesArray[i].assessmentsResults.length; j++) {
        categoriesArray[i].assessmentsResults[j].categoryName =
          categoriesArray[i].categoryName;
      }
    }
    // Merge all assessments in on array (into assessmentResult of first category)
    for (let i = 1; i < categoriesArray.length; i++) {
      categoriesArray[0].assessmentsResults.push.apply(
        categoriesArray[0].assessmentsResults,
        categoriesArray[i].assessmentsResults
      );
    }
    console.log(categoriesArray);
    return categoriesArray[0].assessmentsResults;
  };

  const classes = useStyles();
  const fixedHeightPaper = clsx(classes.paper, classes.fixedHeightPaper);

  return (
    <div className={classes.appBarSpacer}>
      <Container maxWidth="lg" className={classes.container}>
        <div style={{ maxWidth: "100%" }}>
          <MaterialTable
            icons={tableIcons}
            columns={[
              { title: "Category Name", field: "categoryName" },
              { title: "Comment", field: "assessmentComment" },
              { title: "Score", field: "assessmentScore", type: "numeric" },
              {
                title: "Date",
                field: "assessmentDate",
                type: "date"
              }
            ]}
            options={{
              searchText: props.searchText
            }}
            data={fitData(props.fetchResult)}
            title="Assessment History"
          />
        </div>
      </Container>
    </div>
  );
}
