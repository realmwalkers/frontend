import React from "react";
import Dashboard from "./Dashboard";

export default class DashboardContainer extends React.Component {
  state = {
    fetchResult: null,
    fetchResultText: ""
  };
  async componentDidMount() {
    await this.fetchData(7);
  }

  handleDayChange = async days => {
    await this.fetchData(days);
  };

  fetchData = async days => {
    let categoryId = this.props.match.params.categoryId;
    if (categoryId) {
      try {
        let fetchAssessmentData = await fetch(
          `${process.env.REACT_APP_API_URL}/assessment/history?categoryId=${categoryId}&days=${days}`,
          {
            credentials: "include"
          }
        );
        if (fetchAssessmentData.ok) {
          let text = await fetchAssessmentData.text();
          console.log(JSON.parse(text));
          await this.setState({
            fetchResult: JSON.parse(text),
            fetchResultText: text
          });
        }
      } catch (err) {
        console.log(err);
      }
    } else {
      let fetchCategoryResp = await fetch(
        `${process.env.REACT_APP_API_URL}/category`,
        {
          credentials: "include",
          headers: {
            "Content-Type": "application/json"
          }
        }
      );
      if (fetchCategoryResp.ok) {
        let categoryJson = await fetchCategoryResp.json();
        if (categoryJson.length > 0) {
          let categoryId = categoryJson[0].categoryId;
          let fetchAssessmentData = await fetch(
            `${process.env.REACT_APP_API_URL}/assessment/history?categoryId=${categoryId}&days=${days}`,
            {
              credentials: "include"
            }
          );
          if (fetchAssessmentData.ok) {
            let text = await fetchAssessmentData.text();
            await this.setState({
              fetchResult: JSON.parse(text),
              fetchResultText: text
            });
          }
        }
      }
    }
  };
  render() {
    return (
      <Dashboard
        {...this.props}
        handleDayChange={this.handleDayChange}
        fetchResult={this.state.fetchResult}
        fetchResultText={this.state.fetchResultText}
      />
    );
  }
}
