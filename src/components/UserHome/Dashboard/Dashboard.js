import React from "react";
import { Grid, Container, Paper, makeStyles } from "@material-ui/core";
import clsx from "clsx";
import Score from "./Score";
import RecentScore from "./RecentScore";
import LineGraph from "./LineGraph";

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex"
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: "100vh",
    overflow: "auto"
  },
  container: {
    marginLeft: 57,
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4)
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column"
  },
  graphPaperHeight: {
    height: 500 - 24
  },
  fixedHeightPaper: {
    height: 250 - 24
  }
}));

export default function Dashboard(props) {
  const classes = useStyles();
  const fixedHeightPaper = clsx(classes.paper, classes.fixedHeightPaper);
  return (
    <div className={classes.appBarSpacer}>
      <Container maxWidth="lg" className={classes.container}>
        <Grid container spacing={3}>
          <Grid item xs={12} md={4} lg={3}>
            <Paper className={classes.fixedHeightPaper}>
              <Score fetchResult={props.fetchResult} />
            </Paper>
            <Paper
              className={classes.fixedHeightPaper}
              style={{ marginTop: "24px" }}
            >
              <RecentScore {...props} fetchResult={props.fetchResult} />
            </Paper>
          </Grid>
          <Grid item xs={12} md={8} lg={9}>
            <Paper className={classes.graphPaperHeight}>
              <LineGraph
                {...props}
                handleDayChange={props.handleDayChange}
                fetchResult={props.fetchResult}
                fetchResultText={props.fetchResultText}
              />
            </Paper>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
}
