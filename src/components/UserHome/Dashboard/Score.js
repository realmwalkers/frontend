import React from "react";
import Link from "@material-ui/core/Link";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Title from "./Title";

export default class Score extends React.Component {
  state = {
    score: 0,
    dimArr: []
  };
  componentDidUpdate(prevProps) {
    if (prevProps !== this.props) {
      this.updateCategoryScore();
      this.calcDimensionScores();
    }
  }

  updateCategoryScore() {
    if (this.props.fetchResult) {
      if (this.props.fetchResult.categoryScoreTime !== "NaN") {
        this.setState({ score: this.props.fetchResult.categoryScoreTime });
      }
    }
  }

  calcDimensionScores() {
    if (this.props.fetchResult) {
      let objArr = this.createDimensionObjects(
        this.props.fetchResult.dimensionsTemplate
      );
      if (this.props.fetchResult.assessmentsResults) {
        let assessArr = this.props.fetchResult.assessmentsResults;
        for (var i = 0; i < assessArr.length; i++) {
          let dimensionArr = assessArr[i].dimensionResults;
          for (var j = 0; j < dimensionArr.length; j++) {
            for (var k = 0; k < objArr.length; k++) {
              if (dimensionArr[j].dimensionName === objArr[k].dimName) {
                objArr[k].dataArr.push(dimensionArr[j].dimensionScore);
              }
            }
          }
        }
      } else {
        for (var k = 0; k < objArr.length; k++) {
          objArr[k].dataArr.push(0);
        }
      }
      console.log(objArr);
      this.setState({ dimArr: objArr });
    }
  }

  createDimensionObjects = dimTemplate => {
    let objArr = [];
    let dimensionsTemplate = dimTemplate;
    for (var i = 0; i < dimensionsTemplate.length; i++) {
      let dim = { dimName: "", dataArr: [] };
      dim.dimName = dimensionsTemplate[i].dimensionName;
      objArr.push(dim);
    }
    return objArr;
  };

  renderScore(arr) {
    if (arr) {
      if (arr.length > 0) {
        return Math.ceil(
          (arr.reduce((a, b) => a + b, 0) / (arr.length * 10)) * 100
        );
      } else {
        return 0;
      }
    } else {
      return 0;
    }
  }

  render() {
    return (
      <React.Fragment>
        <Title>Category Score</Title>
        <Typography component="p" variant="h3">
          {`${this.state.score}%`}
        </Typography>
        <Title>Dimension Score</Title>
        {this.state.dimArr.map(dim => {
          return (
            <Typography component="p">
              {dim.dimName} - {this.renderScore(dim.dataArr)} %
            </Typography>
          );
        })}
      </React.Fragment>
    );
  }
}
