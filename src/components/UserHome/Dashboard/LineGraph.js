import React from "react"
import * as Highcharts from "highcharts"
import HighchartsReact from "highcharts-react-official"
import Button from "@material-ui/core/Button"
import "./style.css"

class LineGraph extends React.Component {
  state = {
    categoryId: 4.514530544246746,
    chartOptions: {
      chart: {
        zoomType: "x",
        type: "spline",
        backgroundColor: "#424242",
        time: {
          timezoneOffset: 240
        }
      },
      title: {
        text: "Line Graph",
        style: {
          color: "#FFFFFF"
        }
      },
      credits: {
        enabled: false
      },
      labels: {
        style: {
          color: "#707073"
        }
      },
      xAxis: {
        type: "datetime",
        dateTimeLabelFormats: {
          // don't display the dummy year
          month: "%e. %b",
          year: "%b"
        },
        title: {
          text: "Date",
          style: {
            color: "#FFFFFF"
          }
        },
        labels: {
          style: {
            color: "#FFFFFF"
          }
        }
      },
      yAxis: {
        title: {
          style: {
            color: "#FFFFFF"
          },
          text: "Rating"
        },
        min: 0,
        max: 10,
        tickInterval: 2,
        gridLineWidth: 0,
        gridLineColor: "#707073",
        labels: {
          style: {
            color: "#FFFFFF"
          }
        }
      },
      tooltip: {
        headerFormat: "<b>{series.name}</b><br>",
        pointFormat: "{point.x:%e. %b}: {point.y:.2f}"
      },
      legend: {
        // backgroundColor: "rgba(0, 0, 0, 0.5)",
        itemStyle: {
          color: "#E0E0E3"
        },
        itemHoverStyle: {
          color: "#FFF"
        },
        itemHiddenStyle: {
          color: "#606063"
        },
        title: {
          style: {
            color: "#C0C0C0"
          }
        }
      },
      plotOptions: {
        series: {
          dataLabels: {
            color: "#F0F0F3",
            style: {
              fontSize: "13px"
            }
          },
          marker: {
            enabled: true
          },
          cursor: "pointer",
          events: {
            click: (e) => this.redirectToAssessment(e)
          }
        }
      },
      colors: ["#6CF", "#39F", "#06C", "#036", "#000"],
      series: [],
      responsive: {
        rules: [
          {
            condition: {
              maxWidth: 500
            },
            chartOptions: {
              plotOptions: {
                series: {
                  marker: {
                    radius: 2.5
                  }
                }
              }
            }
          }
        ]
      }
    }
  }

  redirectToAssessment = (e) => {
    let dateISO = new Date(e.point.x).toISOString().split("T")[0]
    let rating = e.point.y
    this.props.history.push(`/history/${dateISO}/${rating}`)
  }

  getTitle = () => {
    // let titleStr = this.props.match.params.categoryName;
    if (this.props.fetchResult.categoryName) {
      return this.props.fetchResult.categoryName
    } else {
      return ""
    }
  }

  async componentDidUpdate(prevProps) {
    if (prevProps.fetchResult !== this.props.fetchResult) {
      if (this.props.fetchResultText) {
        let json = JSON.parse(this.props.fetchResultText)
        console.log(json)
        let lineGraphData = this.getLineGraphData(
          JSON.parse(this.props.fetchResultText)
        )
        this.setState({
          chartOptions: {
            series: lineGraphData,
            title: {
              text: this.getTitle()
            }
          }
        })
      } else {
        let lineGraphData = this.getLineGraphData(this.props.fetchResult)
        this.setState({
          chartOptions: {
            series: lineGraphData,
            title: {
              text: this.getTitle()
            }
          }
        })
      }
    }
  }

  getSeriesArray = (graphData) => {
    let seriesArr = []
    for (var i = 0; i < graphData.length; i++) {
      seriesArr.push({
        name: graphData[i].dimName,
        data: graphData[i].dataArr
      })
    }
    return seriesArr
  }

  createDimensionObjects = (dimTemplate) => {
    let objArr = []
    let dimensionsTemplate = dimTemplate
    for (var i = 0; i < dimensionsTemplate.length; i++) {
      let dim = { dimName: "", dataArr: [] }
      dim.dimName = dimensionsTemplate[i].dimensionName
      objArr.push(dim)
    }
    return objArr
  }

  getLineGraphData = (result) => {
    let objArr = this.createDimensionObjects(result.dimensionsTemplate)
    let assessmentArr = result.assessmentsResults
    for (var i = 0; i < assessmentArr.length; i++) {
      let dimensionArr = assessmentArr[i].dimensionResults
      let date = new Date(assessmentArr[i].assessmentDate)
      for (var j = 0; j < dimensionArr.length; j++) {
        let utc = Date.UTC(
          date.getUTCFullYear(),
          date.getUTCMonth(),
          date.getUTCDate(),
          date.getUTCHours(),
          date.getUTCMinutes(),
          date.getUTCSeconds()
        )
        for (var k = 0; k < objArr.length; k++) {
          if (dimensionArr[j].dimensionName === objArr[k].dimName) {
            objArr[k].dataArr.push([utc, dimensionArr[j].dimensionScore])
          }
        }
      }
    }
    for (var i = 0; i < objArr.length; i++) {
      objArr[i].dataArr.sort()
    }
    let seriesData = this.getSeriesArray(objArr)
    return seriesData
  }

  handleClick = async (event) => {
    let timeStr = event.target.innerHTML
    let days
    if (timeStr === "Year") {
      days = 365
    } else if (timeStr === "Month") {
      days = 30
    } else {
      days = 7
    }
    this.props.handleDayChange(days)
  }

  render() {
    return (
      <div>
        <Button
          id='7'
          onClick={this.handleClick}
          variant='primary'
          selected={true}
        >
          Week
        </Button>
        <Button id='30' onClick={this.handleClick} variant='primary'>
          Month
        </Button>
        <Button id='365' onClick={this.handleClick} variant='primary'>
          Year
        </Button>
        <HighchartsReact
          highcharts={Highcharts}
          options={this.state.chartOptions}
        />
      </div>
    )
  }
}
export default LineGraph
