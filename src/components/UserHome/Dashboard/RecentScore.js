import React from "react"
import Link from "@material-ui/core/Link"
import { makeStyles } from "@material-ui/core/styles"
import Typography from "@material-ui/core/Typography"
import Title from "./Title"
import arrowUp from "../../../arrowUp.png"
import arrowDown from "../../../arrowDown.png"

export default class RecentScore extends React.Component {
  state = {
    score: 0,
    date: ""
  }
  componentDidUpdate(prevProps) {
    if (prevProps !== this.props) {
      if (this.props.fetchResult) {
        if (this.props.fetchResult.assessmentsResults.length > 0) {
          this.setState({
            score: this.props.fetchResult.differenceScoreTime,
            date: this.props.fetchResult.assessmentsResults
              .pop()
              .assessmentDate.split("T")[0]
          })
        } else {
          this.setState({
            score: this.props.fetchResult.differenceScoreTime
          })
        }
      }
    }
  }

  renderScore = () => {
    let { score } = this.state
    let trendDirection = arrowUp
    if (score < 0) {
      trendDirection = arrowDown
    }
    return (
      <>
        <img src={trendDirection} alt='Progress trend' width='25' height='25' />{" "}
        {Math.abs(this.state.score)}%
      </>
    )
  }

  renderLastAssessment = () => {
    if (this.state.date) {
      return `on ${this.state.date}`
    } else {
      return "No assessments"
    }
  }
  render() {
    return (
      <React.Fragment>
        <Title>Last Assessment</Title>
        <Typography component='p' variant='h4'>
          {this.renderScore()}
        </Typography>
        <Typography color='textSecondary' style={{ flex: 1 }}>
          {this.renderLastAssessment()}
        </Typography>
        <div>
          <Link
            color='primary'
            onClick={() => this.props.history.push("/history")}
          >
            View history
          </Link>
        </div>
      </React.Fragment>
    )
  }
}
