import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import StepContent from "@material-ui/core/StepContent";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import { RadioGroup, Radio, FormControlLabel } from "@material-ui/core";
const useStyles = makeStyles(theme => ({
  root: {
    width: "100%"
  },
  button: {
    marginTop: theme.spacing(1),
    marginRight: theme.spacing(1),
    alignItems: "center"
  },
  actionsContainer: {
    marginBottom: theme.spacing(2)
  },
  resetContainer: {
    padding: theme.spacing(3)
  }
}));

function getSteps(props) {
  return props.formData["Items"];
}

export default function VerticalLinearStepper(props) {
  console.log(props);
  const classes = useStyles();
  const steps = getSteps(props);
  const [activeStep, setActiveStep] = React.useState(0);
  const [tScore, setTScore] = React.useState("");
  const [currResp, setCurrResp] = React.useState({
    ItemResponseOID: "",
    Response: ""
  });

  const handleChange = event => {
    console.log(event.target);
    setCurrResp({
      ItemResponseOID: event.target.name,
      Response: event.target.value
    });
  };

  React.useEffect(() => {});

  const handleNext = async () => {
    console.log(currResp);
    if (currResp.ItemResponseOID !== "") {
      let answerResp = await fetch(
        `https://www.assessmentcenter.net/ac_api/2014-01/Participants/${props.assessmentOID}.json?ItemResponseOID=${currResp.ItemResponseOID}&Response=${currResp.Response}`,
        {
          headers: {
            Authorization:
              "Basic " +
              "NEM2MEUxQzUtRTkxMS00NTRGLUJDOTYtODk2RTIzOEI3Q0UyOkMzQzIyQTBELUVBNjAtNDI2Ni1CQTQ0LTcwQ0UzNUEzNjQwMQ=="
          }
        }
      );
      console.log(await answerResp.json());
      setActiveStep(prevActiveStep => prevActiveStep + 1);
      setCurrResp({
        ItemResponseOID: "",
        Response: ""
      });
    }
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  const handleGetResult = async () => {
    let answerResp = await fetch(
      `https://www.assessmentcenter.net/ac_api/2014-01/Results/${props.assessmentOID}.json`,
      {
        headers: {
          Authorization:
            "Basic " +
            "NEM2MEUxQzUtRTkxMS00NTRGLUJDOTYtODk2RTIzOEI3Q0UyOkMzQzIyQTBELUVBNjAtNDI2Ni1CQTQ0LTcwQ0UzNUEzNjQwMQ=="
        }
      }
    );
    let resultJson = await answerResp.json();
    let tScore = resultJson.Theta * 10 + 50;
    console.log(tScore);
    setTScore(tScore);
  };

  const renderTScore = () => {
    if (tScore) {
      return `T-Score: ${tScore.toFixed(0)}`;
    } else {
      return "";
    }
  };

  return (
    <div className={classes.root}>
      <Stepper activeStep={activeStep} orientation="vertical">
        {steps.map((step, index) => (
          <Step key={step.ID}>
            <StepLabel>{`Question ${step.Order}`}</StepLabel>
            <StepContent>
              <Typography>{step.Elements[0].Description}</Typography>
              <div className={classes.actionsContainer}>
                {/* RESPONSES */}
                <RadioGroup
                  aria-label="answers"
                  name="answers"
                  value={currResp.Response}
                  style={{ display: "inline-block" }}
                  onChange={handleChange}
                >
                  {step.Elements[1].Map.map(answer => (
                    <FormControlLabel
                      name={answer.ItemResponseOID}
                      value={answer.Value}
                      control={<Radio color="primary" />}
                      label={answer.Description}
                      labelPlacement="bottom"
                    />
                  ))}
                </RadioGroup>
                <div>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={handleNext}
                    className={classes.button}
                    disabled={currResp.ItemResponseOID === ""}
                  >
                    {activeStep === steps.length - 1 ? "Finish" : "Next"}
                  </Button>
                </div>
              </div>
            </StepContent>
          </Step>
        ))}
      </Stepper>
      {activeStep === steps.length && (
        <Paper square elevation={0} className={classes.resetContainer}>
          <Typography>{}</Typography>
          <Button onClick={handleGetResult} className={classes.button}>
            Get Results
          </Button>
          <p>{renderTScore()}</p>
        </Paper>
      )}
    </div>
  );
}
