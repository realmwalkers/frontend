import React from "react";
import PromisApiStepper from "./PromisApiStepper";
export default class Promis extends React.Component {
  state = {
    isDataFetched: false,
    assessmentOID: null,
    formData: null
  };

  async componentDidMount() {
    let data = await this.fetchGlobalMentalHealthForm();
    let oid = await this.fetchRegisterForm();
    await this.fetchParticipant(oid);
    this.setState({ isDataFetched: true, assessmentOID: oid, formData: data });
  }

  fetchParticipant = async assessmentOID => {
    let participantResp = await fetch(
      `https://www.assessmentcenter.net/ac_api/2014-01/Participants/${assessmentOID}.json`,
      {
        headers: {
          Authorization:
            "Basic " +
            "NEM2MEUxQzUtRTkxMS00NTRGLUJDOTYtODk2RTIzOEI3Q0UyOkMzQzIyQTBELUVBNjAtNDI2Ni1CQTQ0LTcwQ0UzNUEzNjQwMQ=="
        }
      }
    );
    console.log("PARTICIPANT FETCH");
    console.log(participantResp.ok);
    console.log(await participantResp.json());
  };

  fetchRegisterForm = async () => {
    let registerAssessmentResp = await fetch(
      "https://www.assessmentcenter.net/ac_api/2014-01/Assessments/F37B59EF-E6E1-4356-9FF1-AD349B689012.json",
      {
        headers: {
          Authorization:
            "Basic " +
            "NEM2MEUxQzUtRTkxMS00NTRGLUJDOTYtODk2RTIzOEI3Q0UyOkMzQzIyQTBELUVBNjAtNDI2Ni1CQTQ0LTcwQ0UzNUEzNjQwMQ=="
        }
      }
    );
    console.log("REGISTER FETCH");
    console.log(registerAssessmentResp.ok);
    let registerAssessmentRespJson = await registerAssessmentResp.json();
    let assessmentOID = registerAssessmentRespJson.OID;
    return assessmentOID;
  };

  fetchGlobalMentalHealthForm = async () => {
    let getAssessmentResp = await fetch(
      "https://www.assessmentcenter.net/ac_api/2014-01/Forms/F37B59EF-E6E1-4356-9FF1-AD349B689012.json",
      {
        headers: {
          Authorization:
            "Basic " +
            "NEM2MEUxQzUtRTkxMS00NTRGLUJDOTYtODk2RTIzOEI3Q0UyOkMzQzIyQTBELUVBNjAtNDI2Ni1CQTQ0LTcwQ0UzNUEzNjQwMQ=="
        }
      }
    );
    console.log("GET FORM FETCH");
    console.log(getAssessmentResp.ok);
    let formJson = await getAssessmentResp.json();
    return formJson;
  };

  render() {
    if (this.state.isDataFetched) {
      return (
        <PromisApiStepper
          formData={this.state.formData}
          assessmentOID={this.state.assessmentOID}
        />
      );
    }
    return <p>loading</p>;
  }
}
