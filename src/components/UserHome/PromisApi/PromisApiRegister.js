import React from "react";
import PromisApiStepper from "./PromisApiStepper";
export default class Promis extends React.Component {
  async componentDidMount() {
    //starts new assessment
    // let registerAssessmentResp = await fetch(
    //   "https://www.assessmentcenter.net/ac_api/2014-01/Assessments/F37B59EF-E6E1-4356-9FF1-AD349B689012.json",
    //   {
    //     headers: {
    //       Authorization:
    //         "Basic " +
    //         "NEM2MEUxQzUtRTkxMS00NTRGLUJDOTYtODk2RTIzOEI3Q0UyOkMzQzIyQTBELUVBNjAtNDI2Ni1CQTQ0LTcwQ0UzNUEzNjQwMQ=="
    //     }
    //   }
    // );
    // let registerAssessmentRespJson = await registerAssessmentResp.json();
    // let assessmentOID = registerAssessmentRespJson.OID;
    // let participantResp = await fetch(
    //   `https://www.assessmentcenter.net/ac_api/2014-01/Participants/${assessmentOID}.json`,
    //   {
    //     headers: {
    //       Authorization:
    //         "Basic " +
    //         "NEM2MEUxQzUtRTkxMS00NTRGLUJDOTYtODk2RTIzOEI3Q0UyOkMzQzIyQTBELUVBNjAtNDI2Ni1CQTQ0LTcwQ0UzNUEzNjQwMQ=="
    //     }
    //   }
    // );
    // console.log(await participantResp.json());
  }

  render() {
    let mockJson = {
      DateFinished: "",
      Items: [
        {
          FormItemOID: "D623BE3F-6666-4CD6-87E0-3737EA10FF69",
          ItemResponseOID: "",
          Response: "",
          ID: "Global04",
          Order: "1",
          ItemType: "",
          Elements: [
            {
              ElementOID: "A7957776-AE1F-478A-83FD-E7D71BC51FCF",
              Description:
                "In general, how would you rate your mental health, including your mood and your ability to think?",
              ElementOrder: "1"
            },
            {
              ElementOID: "94CE94FB-D45B-46A1-9243-C93470D76349",
              Description: "ContainerFor94CE94FB-D45B-46A1-9243-C93470D76349",
              ElementOrder: "2",
              Map: [
                {
                  ElementOID: "F77EF184-6E39-41F4-BC51-AA34B80D6273",
                  Description: "Excellent",
                  FormItemOID: "D623BE3F-6666-4CD6-87E0-3737EA10FF69",
                  ItemResponseOID: "99404729-AD2D-4391-9E87-F3DA4875DA7A",
                  Value: "5",
                  Position: "1"
                },
                {
                  ElementOID: "6B7D9C69-8D44-48C3-92FF-DF4909D18EA2",
                  Description: "Very good",
                  FormItemOID: "D623BE3F-6666-4CD6-87E0-3737EA10FF69",
                  ItemResponseOID: "785444D8-8454-41AE-8F59-07596F0742A4",
                  Value: "4",
                  Position: "2"
                },
                {
                  ElementOID: "571A1997-AE00-401E-B539-FB66CFA4566B",
                  Description: "Good",
                  FormItemOID: "D623BE3F-6666-4CD6-87E0-3737EA10FF69",
                  ItemResponseOID: "6842FF8F-2240-4250-9FEA-78612DE8C606",
                  Value: "3",
                  Position: "3"
                },
                {
                  ElementOID: "FC614238-1FEE-425F-956B-E1D530EC884E",
                  Description: "Fair",
                  FormItemOID: "D623BE3F-6666-4CD6-87E0-3737EA10FF69",
                  ItemResponseOID: "AF6BD8A0-185C-414F-BBB0-54BEC42454FD",
                  Value: "2",
                  Position: "4"
                },
                {
                  ElementOID: "F2AC0B3F-9430-4B94-AA3E-7D583F19411F",
                  Description: "Poor",
                  FormItemOID: "D623BE3F-6666-4CD6-87E0-3737EA10FF69",
                  ItemResponseOID: "8DFD70C3-15F6-4B16-8829-481E7A202D29",
                  Value: "1",
                  Position: "5"
                }
              ]
            }
          ]
        }
      ]
    };

    let mockJson2 = {
      DateFinished: "",
      Items: [
        {
          FormItemOID: "D623BE3F-6666-4CD6-87E0-3737EA10FF69",
          ItemResponseOID: "",
          Response: "",
          ID: "Global04",
          Order: "1",
          ItemType: "",
          Elements: [
            {
              ElementOID: "A7957776-AE1F-478A-83FD-E7D71BC51FCF",
              Description:
                "In general, how would you rate your mental health, including your mood and your ability to think?",
              ElementOrder: "1"
            },
            {
              ElementOID: "94CE94FB-D45B-46A1-9243-C93470D76349",
              Description: "ContainerFor94CE94FB-D45B-46A1-9243-C93470D76349",
              ElementOrder: "2",
              Map: [
                {
                  ElementOID: "F77EF184-6E39-41F4-BC51-AA34B80D6273",
                  Description: "Excellent",
                  FormItemOID: "D623BE3F-6666-4CD6-87E0-3737EA10FF69",
                  ItemResponseOID: "99404729-AD2D-4391-9E87-F3DA4875DA7A",
                  Value: "5",
                  Position: "1"
                },
                {
                  ElementOID: "6B7D9C69-8D44-48C3-92FF-DF4909D18EA2",
                  Description: "Very good",
                  FormItemOID: "D623BE3F-6666-4CD6-87E0-3737EA10FF69",
                  ItemResponseOID: "785444D8-8454-41AE-8F59-07596F0742A4",
                  Value: "4",
                  Position: "2"
                },
                {
                  ElementOID: "571A1997-AE00-401E-B539-FB66CFA4566B",
                  Description: "Good",
                  FormItemOID: "D623BE3F-6666-4CD6-87E0-3737EA10FF69",
                  ItemResponseOID: "6842FF8F-2240-4250-9FEA-78612DE8C606",
                  Value: "3",
                  Position: "3"
                },
                {
                  ElementOID: "FC614238-1FEE-425F-956B-E1D530EC884E",
                  Description: "Fair",
                  FormItemOID: "D623BE3F-6666-4CD6-87E0-3737EA10FF69",
                  ItemResponseOID: "AF6BD8A0-185C-414F-BBB0-54BEC42454FD",
                  Value: "2",
                  Position: "4"
                },
                {
                  ElementOID: "F2AC0B3F-9430-4B94-AA3E-7D583F19411F",
                  Description: "Poor",
                  FormItemOID: "D623BE3F-6666-4CD6-87E0-3737EA10FF69",
                  ItemResponseOID: "8DFD70C3-15F6-4B16-8829-481E7A202D29",
                  Value: "1",
                  Position: "5"
                }
              ]
            }
          ]
        },
        {
          FormItemOID: "14BE5C9A-D50D-47E6-BF96-F31B40E91DAA",
          ItemResponseOID: "",
          Response: "",
          ID: "Global05",
          Order: "2",
          ItemType: "",
          Elements: [
            {
              ElementOID: "D9C40754-8B48-4166-8DB0-F0DB13AA0F4A",
              Description:
                "In general, how would you rate your satisfaction with your social activities and relationships?",
              ElementOrder: "1"
            },
            {
              ElementOID: "94CE94FB-D45B-46A1-9243-C93470D76349",
              Description: "ContainerFor94CE94FB-D45B-46A1-9243-C93470D76349",
              ElementOrder: "2",
              Map: [
                {
                  ElementOID: "F77EF184-6E39-41F4-BC51-AA34B80D6273",
                  Description: "Excellent",
                  FormItemOID: "14BE5C9A-D50D-47E6-BF96-F31B40E91DAA",
                  ItemResponseOID: "597C42C0-2694-41C9-93C0-50964660BC14",
                  Value: "5",
                  Position: "1"
                },
                {
                  ElementOID: "6B7D9C69-8D44-48C3-92FF-DF4909D18EA2",
                  Description: "Very good",
                  FormItemOID: "14BE5C9A-D50D-47E6-BF96-F31B40E91DAA",
                  ItemResponseOID: "A6BC08D5-9C6D-4256-9465-602EB0541216",
                  Value: "4",
                  Position: "2"
                },
                {
                  ElementOID: "571A1997-AE00-401E-B539-FB66CFA4566B",
                  Description: "Good",
                  FormItemOID: "14BE5C9A-D50D-47E6-BF96-F31B40E91DAA",
                  ItemResponseOID: "4A373FB9-50CE-4ED5-B269-7540BBBAB2A8",
                  Value: "3",
                  Position: "3"
                },
                {
                  ElementOID: "FC614238-1FEE-425F-956B-E1D530EC884E",
                  Description: "Fair",
                  FormItemOID: "14BE5C9A-D50D-47E6-BF96-F31B40E91DAA",
                  ItemResponseOID: "AF25CE37-345D-47C9-BC90-81A0148E3268",
                  Value: "2",
                  Position: "4"
                },
                {
                  ElementOID: "F2AC0B3F-9430-4B94-AA3E-7D583F19411F",
                  Description: "Poor",
                  FormItemOID: "14BE5C9A-D50D-47E6-BF96-F31B40E91DAA",
                  ItemResponseOID: "E9B7708F-7686-4F59-9ACB-B078990C7F6C",
                  Value: "1",
                  Position: "5"
                }
              ]
            }
          ]
        }
      ]
    };
    return <PromisApiStepper mockData={mockJson2} />;
  }
}
