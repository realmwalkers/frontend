import React from "react"
import {
  FormGroup,
  Typography,
  TextField,
  Button,
  Link,
  Card
} from "@material-ui/core"
import { Redirect, NavLink } from "react-router-dom"
import Particles from "react-particles-js"
import dataTraceLogo2 from "../../dataTraceLogo2.png"

export default class Register extends React.Component {
  state = {
    isUserValidated: false
  }

  handleSubmit = async (event) => {
    event.preventDefault()
    let email = event.target.email.value
    let password = event.target.password.value
    let postBody = {
      email: email,
      password: password
    }
    let registerResp = await fetch(
      `${process.env.REACT_APP_API_URL}/user/signup`,
      {
        headers: {
          "Content-Type": "application/json"
        },
        method: "POST",
        body: JSON.stringify(postBody)
      }
    )
    console.log(registerResp.status)
    if (registerResp.status === 200) {
      alert("Registered successfully. An email verification has been sent.")
      this.setState({ isUserValidated: true })
    }
  }

  render() {
    if (this.state.isUserValidated) {
      return <Redirect to='/login' />
    } else {
      return (
        <>
          <Particles
            className='particles-bg-styling'
            params={{
              particles: {
                number: {
                  value: 80,
                  density: {
                    // enable: true,
                    value_area: 800
                  }
                },
                size: {
                  value: "1"
                },
                // color: {
                //   value: "#ffffff"
                // },
                line_linked: {
                  color: "#2196f3",
                  opacity: 1
                }
              }
            }}
          />
          <div className='login-card-margin-top'>
            <Card className='login-page-card-style'>
              <Typography
                style={{
                  marginTop: "16px",
                  fontSize: "80px",
                  fontWeight: "700"
                }}
                variant='h2'
              >
                <br />
                <span className='D'>D</span>ata<span className='T'>T</span>
                race{" "}
                <img
                  className='login-Page-logo-styling'
                  src={dataTraceLogo2}
                  alt='Data Trace Logo'
                  width='75'
                  height='75'
                />
              </Typography>
              <div
                style={{
                  marginTop: "44px",
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center"
                }}
              >
                <form style={{ width: "50%" }} onSubmit={this.handleSubmit}>
                  <FormGroup>
                    <TextField
                      variant='outlined'
                      margin='normal'
                      required
                      fullWidth
                      id='email'
                      label='Email Address'
                      name='email'
                      autoComplete='email'
                      autoFocus
                    />
                    <TextField
                      variant='outlined'
                      margin='normal'
                      required
                      fullWidth
                      name='password'
                      label='Password'
                      type='password'
                      id='password'
                      autoComplete='current-password'
                    />
                    <Button type='submit' variant='outlined'>
                      Sign up
                    </Button>
                    <Typography>
                      <NavLink to='/login'>
                        <br />
                        <Link>
                          Already a user? Click here to login
                        </Link> <br /> <br /> <br />{" "}
                      </NavLink>
                    </Typography>
                  </FormGroup>
                </form>
              </div>
            </Card>
          </div>
        </>
      )
    }
  }
}
