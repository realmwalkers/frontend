import React from "react"
import {
  FormGroup,
  Typography,
  TextField,
  Button,
  Link,
  Card
} from "@material-ui/core"
import auth from "../../auth"
import { Redirect, NavLink } from "react-router-dom"
import "./index.css"
import dataTraceLogo from "../../dataTraceLogo.svg"
import dataTraceLogo2 from "../../dataTraceLogo2.png"
import Particles from "react-particles-js"

export default class Login extends React.Component {
  state = {
    isUserValidated: false
  }

  handleSubmit = async (event) => {
    event.preventDefault()
    console.log("submitted")
    let email = event.target.email.value
    let password = event.target.password.value
    let postBody = {
      email: email,
      password: password
    }
    let validResp = await fetch(`${process.env.REACT_APP_API_URL}/user/login`, {
      credentials: "include",
      headers: {
        "Content-Type": "application/json"
      },
      method: "POST",
      body: JSON.stringify(postBody)
    })
    if (validResp.status === 200) {
      auth.login()
      this.setState({ isUserValidated: true })
    } else {
      alert("Invalid credentials. Please try again.")
    }
  }

  render() {
    if (this.state.isUserValidated) {
      return <Redirect to='/dashboard' />
    } else {
      return (
        <>
          <Particles
            className='particles-bg-styling'
            params={{
              particles: {
                number: {
                  value: 80,
                  density: {
                    // enable: true,
                    value_area: 800
                  }
                },
                // size: {
                //   value: "1"
                // },
                // color: {
                //   value: "#ffffff"
                // },
                line_linked: {
                  color: "#2196f3",
                  opacity: 1
                }
              }
            }}
          />
          <div className='login-card-margin-top'>
            <Card className='login-page-card-style'>
              <Typography
                style={{
                  marginTop: "16px",
                  fontSize: "80px",
                  fontWeight: "700"
                }}
                variant='h2'
              >
                <br />
                <span>
                  <span className='D'>D</span>ata<span className='T'>T</span>
                  race{" "}
                  <img
                    className='login-Page-logo-styling'
                    src={dataTraceLogo2}
                    alt='Data Trace Logo'
                    width='75'
                    height='75'
                  />
                </span>
              </Typography>
              <div
                style={{
                  marginTop: "44px",
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center"
                }}
              >
                <form style={{ width: "50%" }} onSubmit={this.handleSubmit}>
                  <FormGroup>
                    <TextField
                      variant='outlined'
                      margin='normal'
                      required
                      fullWidth
                      id='email'
                      label='Email Address'
                      name='email'
                      autoComplete='email'
                      autoFocus
                    />
                    <TextField
                      variant='outlined'
                      margin='normal'
                      required
                      fullWidth
                      name='password'
                      label='Password'
                      type='password'
                      id='password'
                      autoComplete='current-password'
                    />
                    <Button type='submit' variant='outlined'>
                      Log in
                    </Button>
                    <br />
                    <Typography>
                      <NavLink to='/register'>
                        <Link>New? Click here to register </Link> <br /> <br />{" "}
                        <br />
                      </NavLink>
                    </Typography>
                  </FormGroup>
                </form>
              </div>
            </Card>
          </div>
        </>
      )
    }
  }
}
