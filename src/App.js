import React from "react"
import {
  CssBaseline,
  createMuiTheme,
  MuiThemeProvider
} from "@material-ui/core"
import blue from "@material-ui/core/colors/blue"
import { BrowserRouter as Router, Switch, Route } from "react-router-dom"
import { Container } from "@material-ui/core"
import { UserHomeContainer } from "./components/UserHome/UserHomeContainer"
import { LoginContainer } from "./components/Login/LoginContainer"
import Particles from "react-particles-js"
import "./App.css"

const theme = createMuiTheme({
  palette: {
    primary: blue,
    type: "dark"
  },
  spacing: 8
})
function App() {
  return (
    <Router>
      <MuiThemeProvider theme={theme}>
        <CssBaseline />
        <Container>
          <Switch>
            <Route
              exact
              path='/(login|register|/)'
              component={LoginContainer}
            />
            <Route component={UserHomeContainer} />
          </Switch>
        </Container>
      </MuiThemeProvider>
    </Router>
  )
}

export default App
